import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Board {
	
	ArrayList<ArrayList<Cell>> _cells;
	int _size;
	ArrayList<Path> _paths;
	Path _path;
	HashMap<Cell, ArrayList<Cell>> _linkedCell;
	int numDepla;
	
	public static void main(String[] args) {
		try {
			
			Board board = new Board(6);
			board.solve();
			
		} catch (Exception e) {//IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private Cell getCell(int i, int j) {
		Cell cell = _cells.get(i).get(j);
		return cell;
	}

	public Board(int size) {
		_size = size;
		_cells = new ArrayList<ArrayList<Cell>>();
		_paths = new ArrayList<Path>();
		for (int i=0; i<_size; i++) {
			ArrayList<Cell> lineCells = new ArrayList<Cell>(); 
			for (int j=0; j<_size; j++) {
				Cell cell = new Cell(i, j);
				lineCells.add(cell);
			}
			_cells.add(lineCells);
		}
		
		computeLinkedCells();
	}
	
	private void computeLinkedCells() {
		
		int gridSize = _cells.size();
		int size = gridSize -1;
		
		//Pour chaque ligne de cellule
		for(ArrayList<Cell> cellLine : _cells) {
			//Pour chaque cellule dans une ligne
			for(int numCell = 0; numCell < gridSize; numCell++) {
				
				Cell currentCell = cellLine.get(numCell);
				
				//Si la cellule est dans la matrice
				if(currentCell._x - 1 >= 0 && currentCell._y + 2 <= size) {
					addPossibleCell(currentCell, currentCell._x - 1, currentCell._y + 2);
				}
				if(currentCell._x + 1 <= size && currentCell._y + 2 <= size) {
					addPossibleCell(currentCell, currentCell._x + 1, currentCell._y + 2);
				}
				if(currentCell._x + 2 <= size && currentCell._y + 1 <= size) {
					addPossibleCell(currentCell, currentCell._x + 2, currentCell._y + 1);
				}
				if(currentCell._x + 2 <= size && currentCell._y - 1 >= 0) {
					addPossibleCell(currentCell, currentCell._x + 2, currentCell._y - 1);
				}
				if(currentCell._x + 1 <= size && currentCell._y - 2 >= 0) {
					addPossibleCell(currentCell, currentCell._x + 1, currentCell._y - 2);
				}
				if(currentCell._x - 1 >= 0 && currentCell._y - 2 >= 0) {
					addPossibleCell(currentCell, currentCell._x - 1, currentCell._y - 2);
				}
				if(currentCell._x - 2 >= 0 && currentCell._y - 1 >= 0) {
					addPossibleCell(currentCell, currentCell._x - 2, currentCell._y - 1);
				}
				if(currentCell._x - 2 >= 0 && currentCell._y + 1 <= size) {
					addPossibleCell(currentCell, currentCell._x - 2, currentCell._y + 1);
				}
			}
		}
	}
	
	private void addPossibleCell(Cell cell, int x, int y) {
		//On ajoute la cellule lie a la cellule teste
		cell._possibleCells.add(getCell(x, y));
	}

	private void solve() {
		//Nombre de dplacement raliss
		numDepla = 1;
		
		//Cellule de dpart
		Cell cellStart = getCell(0,0);
		
		//Initialisation du chemin
		_path = new Path(this, null, cellStart);
		
		//Si la fonction rcursive trouve une possibilite elle renvoi un message de succs
		if(solveLoop(_path, numDepla)) {
			System.out.println("Resolution termine avec succs.");
		}
		//Sinon elle annonce qu'aucune solution n'a t trouve.
		else {
			System.out.println("Aucune solution trouve.");
		}
	}
	
	private Boolean solveLoop(Path currentPath, int num) {
		//Cellule contenant la prochaine cellule
		Cell nextCell;

		//Si le nombre de dplacement est gal  la taille de la matrice et que la premire et la dernire cellule sont lie
		if(num == (_size * _size) && areLinked(currentPath._newCell, currentPath._startCell)) {
			//On affiche le chemin et on renvoi true
			System.out.println(currentPath);
			return true;
		}
		
		//Pour toutes les cellules lies  la nouvelle cellule
		for(int cell = 0; cell < currentPath._newCell._possibleCells.size(); cell++) {
			
			//On initialise la cellule suivante
			nextCell = currentPath._newCell._possibleCells.get(cell);
			
			//Si la cellule suivante n'a pas encore t visite
			if(!isVisited(currentPath, nextCell)) {
				//On creer le nouveau chemin avec cette cellule
				currentPath = new Path(this, currentPath, nextCell);
				
				//On relance la boucle rcursive avec le nouveau chemin
				if(solveLoop(currentPath, num + 1)) {
					_path = currentPath;
					return true;
				}
				//Si il n'y a pas de possibilite on reviens  l'tape prcedante
				else {
					currentPath = currentPath._pathPred;
				}
			}
		}
		return false;
	}
	
	
	
	private boolean areLinked(Cell cellFrom, Cell cellTo) {
		boolean linked = false;
		
		if(cellFrom._possibleCells.contains(cellTo)) {
			linked = true;
		}
		
	    return linked;
	}
	
	private boolean isVisited(Path path, Cell cell) {
		int x = cell._x;
		int y = cell._y;
		boolean visited = path._visited[x][y];
		return visited;
	}
}
