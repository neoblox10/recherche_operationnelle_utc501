import java.util.ArrayList;

public class Cell {
	
	int _x;
	int _y;
	ArrayList<Cell> _possibleCells;

	public Cell(int x, int y) {
		_x = x;
		_y = y;
		_possibleCells = new ArrayList<Cell>();
	}
	
	@Override
	public String toString() {
		String abcdef = "ABCDEFGH";
		return "[" + abcdef.charAt(_x) + "" + _y + "]";
	}

}