//variables
int N = 6;
int numberToFind = 1045;
int numMax = 111;

//ranges
range range1aN = 1..N;
range range1aNSquared = 1..N*N;

//coordonees des cases speciales
int tab_x[range1aN] = [1, 2, 3, 4, 5, 6];
int tab_y[range1aN] = [5, 3, 6, 4, 1, 2];

dvar int tab[range1aN][range1aN] in range1aNSquared;
dvar int tabBis[range1aN][range1aN] in 1..10000;

//calcul de la division entiere et du reste de la division
int res = numberToFind - numMax ;
int resultDivisionDecimal = res div(N);
int resultDivisionInteger = res % N;

using CP; // important pour resoudre ce genre de problemes tres contraints.

maximize sum(line in range1aN) tab[tab_x[line]][tab_y[line]] + resultDivisionDecimal + resultDivisionInteger;
  
subject to {

	//toutes les lignes doivent etre egales au maximum et toutes les valeurs doivent etre differentes
	forall(column in range1aN){
	  sum(line in range1aN) tab[line][column] == numMax;
	  allDifferent(all (line in range1aN) tab[line][column]);
	}

	//toutes les colonnes doivent etre egales au maximum et toutes les valeurs doivent etre differentes
	forall(line in range1aN){
	  sum(column in range1aN) tab[line][column] == numMax;
	  allDifferent(all (column in range1aN) tab[line][column]);
	}
	
	//toutes les cases doivent etre differentes dans la grille
	forall(line in range1aN){
	  forall(column in range1aN){
	    allDifferent(all (line in range1aN, column in range1aN) tab[line][column]);
	  }
	}
	
	//si la case testee est une case speciale on ajoute dans tableauBis le resultat de la division entiere et le reste
	//sinon on ajoute uniquement le resultat de la division entiere
	forall(column in range1aN){
	  forall(line in range1aN){
	    if((line == 1 && column == 5) || (line == 2 && column == 3) || (line == 3 && column == 6) || (line == 4 && column == 4) || (line == 5 && column == 1) || (line == 6 && column == 2)){
	      tabBis[line][column] == tab[line][column] + resultDivisionInteger + resultDivisionDecimal;
	    }
	    else{
	      tabBis[line][column] == tab[line][column] + resultDivisionDecimal;
	    }
	  }
	}
}